import os
import numpy as np
import dill as pickle
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
import tensorflow as tf
from helper import URL_PREFIX, CSV100K, CSV1M, getTrainTestDataset, AGE_NAME
from utiles.trainningResultsUtiles import *


def maskMatrix(originalMatrix: np.array, quantileRate: float)->np.array:
    
#     try:
#         mean = np.mean(originalMatrix - np.diag(np.diag(originalMatrix)))
#     except:
#         mean = np.mean(originalMatrix)
    quantile = np.quantile(originalMatrix, quantileRate)
    matrixMasked = originalMatrix.copy()
    mask = originalMatrix<=quantile
    matrixMasked[mask]=0
    return matrixMasked, mask

def plotMatrixHeatMap(originalMatrix, maskedMatrix, name):
    
    plt.figure(figsize=(22,8))
    plt.subplot(1,2,1)
    sns.heatmap(originalMatrix, cmap="Blues")
    plt.title("Similarity of " +name+ " - Original", fontsize = 16)
    plt.subplot(1,2,2)
    sns.heatmap(maskedMatrix, cmap="Blues")
    plt.title("Similarity of " +name+ " - Masked", fontsize = 16)

def getOriginalRatingMatrix(fname, estimateMatrix):
    
    ratingsMatrix = np.zeros_like(estimateMatrix)
    ratingDF = pd.read_csv('./datasets/' + fname + '/ratings.csv')
    savePath = './datasets/' + fname + '/originalRatingMatrix.npy'
    if savePath not in walkFiles('./datasets/' + fname, 'npy'):
        if fname != 'ml-1m':
            for index, row in ratingDF.iterrows():
                ratingsMatrix[int(row['userId']),int(row['movieId'])] = row['rating']
        else:
            for index, row in ratingDF.iterrows():
                # Some redundant movies do not appear at rating.csv, so we have to remap the Id to make it continuous at 0.
                # movieIdMap1m = {v: i for i,v in enumerate(tower1m.tfDataset.moviesDF['movieId'])}
                with open('./datasets/ml-1m/movieIdMap.pkl','rb') as f:
                    movieIdMap1m = pickle.load(f)
                ratingsMatrix[int(row['userId']),movieIdMap1m[int(row['movieId'])]] = row['rating']
        np.save(savePath, ratingsMatrix)
    else:
        ratingsMatrix = np.load(savePath)
    return ratingsMatrix

def rmseOnWholeDataset(originalMatrix, estimateMatrix):
    mask = originalMatrix!=0
    mse = np.sum((estimateMatrix[mask]-originalMatrix[mask])**2)/np.sum(mask)
    rmse = np.sqrt(mse)
    return rmse

class SimilarityMatrix(object):
    
    def __init__(self, fname):
        usersSimilarityPath = './datasets/' + fname + '/usersSimilarity.npy'
        moviesSimilarityPath = './datasets/' + fname + '/moviesSimilarity.npy'
        users_moviesSimilarityPath = './datasets/' + fname + '/users_moviesSimilarity.npy'
        files = walkFiles('./datasets/'+fname)
        if usersSimilarityPath in files and moviesSimilarityPath in files and users_moviesSimilarityPath in files:
            self.usersSimilarity = np.load(usersSimilarityPath)
            self.moviesSimilarity = np.load(moviesSimilarityPath)
            self.users_moviesSimilarity = np.load(users_moviesSimilarityPath)
        else:
            tower = Tower(fname)
            self.usersSimilarity = np.dot(tower.usersEmbeddingMatrix, tower.usersEmbeddingMatrix.T)
            self.moviesSimilarity = np.dot(tower.moviesEmbeddingMatrix, tower.moviesEmbeddingMatrix.T)
            users_moviesSimilarity = np.dot(tower.usersEmbeddingMatrix, tower.moviesEmbeddingMatrix.T)
            self.users_moviesSimilarity = users_moviesSimilarity * tower.weightLastLayer + tower.biasLastLayer
#             self.users_moviesSimilarity[self.users_moviesSimilarity>5]=5
            np.save(usersSimilarityPath, self.usersSimilarity)
            np.save(moviesSimilarityPath, self.moviesSimilarity)
            np.save(users_moviesSimilarityPath, self.users_moviesSimilarity)

    def maskSimilarityMatrices(self, quantileRate):
        self.quantileRate = quantileRate
        self.maskedUsersSimilarity, self.Umask = maskMatrix(self.usersSimilarity, quantileRate)
        self.maskedMoviesSimilarity, self.Mmask = maskMatrix(self.moviesSimilarity, quantileRate)
        self.maskedUsers_moviesSimilarity, self.UMmask = maskMatrix(self.users_moviesSimilarity, quantileRate)
        
    def getMatrixPair(self, name):
        """
        name: one of ['Users','Movies', 'Users-Movies'], 'Users-Movies' can be anything except 'Users' and 'Movies'
        """
        if name == 'Users':
            return self.usersSimilarity, self.maskedUsersSimilarity
        elif name == 'Movies':
            return self.moviesSimilarity, self.maskedMoviesSimilarity
        else:
            return self.users_moviesSimilarity, self.maskedUsers_moviesSimilarity
        
    def plotMatrix(self, names = ['Users','Movies', 'Users-Movies']):
        """
        name: combination of ['Users','Movies', 'Users-Movies']
        """
        if isinstance(names, list):
            for name in names:
                original, masked = self.getMatrixPair(name)
                plotMatrixHeatMap(original, masked, name)
        else:
            if names not in ['Users','Movies', 'Users-Movies']:
                raise ValueError('name must from ' + str(['Users','Movies', 'Users-Movies']))
            original, masked = self.getMatrixPair(names)
            plotMatrixHeatMap(original, masked, names)