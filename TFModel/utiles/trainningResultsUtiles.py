import os
import numpy as np
import dill as pickle
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import tensorflow as tf
from helper import URL_PREFIX, CSV100K, CSV1M, getTrainTestDataset, AGE_NAME


tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

def walkFiles(path, suffix=None)->list:
    files_list = []
    for root, dirs, files in os.walk(path):
        for f in files:
            if suffix != None:
                try:
                    if f.split('.')[-1]==suffix:
                        files_list.append(os.path.join(root, f))
                except IndexError:
                    pass
            else:
                files_list.append(os.path.join(root, f))
        for d in dirs:
            pass
    return files_list

def singleFilePath(fname: str, modelName: str, files: list, split=None)-> str:
    for f in files:
        if fname in f and modelName in f:
            if split ==None:
                return f
            else:
                if len(f.split(split))==1:
                    continue
                else:
                    return f.split(split)[0]
                
def getAllPathsByFname(fname, files) -> list:
    paths = []
    for f in files:
        if fname in f:
            paths.append(f)
    
    return paths

def plot(modelName,history_dict, i=None):
    if i!=None:
        plt.subplot(2,3,i)
    train_rmse = history_dict[modelName]['root_mean_squared_error']
    test_rmse = history_dict[modelName]['val_root_mean_squared_error']
    # plt.figure(figsize=(6,18))
    plt.plot(train_rmse)
    plt.plot(test_rmse)
    x, y = test_rmse.index(min(test_rmse)), min(test_rmse)
    plt.scatter(x, y, c='r', marker='x')
    if len(test_rmse)>=50:
        plt.annotate((x,round(y,3)),(x,y),(x-1.5-5-5-5,y+0.08),c='red')
    else:
        plt.annotate((x,round(y,3)),(x,y),(x-1.5,y+0.08),c='red')
    plt.plot([0, x], [y, y], c='b', linestyle='--')
    plt.plot([x, x], [y, 0.8], c='b', linestyle='--')
#     plt.xticks(list(range(1,len(test_rmse) + 1, int(len(test_rmse)/5))))
    plt.title(modelName + ' -- Rmse')
    plt.ylabel('Rmse')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper right')
    if i == None:
        plt.show()

def get_DataSetDF(fname: str) -> pd.DataFrame:
    """

    Args:
        fname: dataset name
    Returns:
        trainDataFrame
    """
    train_path = URL_PREFIX + CSV100K[0]
    test_path = URL_PREFIX + CSV100K[1]
    if fname == 'ml-1m':
        train_path = URL_PREFIX + CSV1M[0]
        test_path = URL_PREFIX + CSV1M[1]
    return pd.read_csv(train_path), pd.read_csv(test_path)

def generateTwoTowerInputCSV(fname):
    
    filesList = walkFiles('./datasets/', 'csv')
    userInputPath = './datasets/'+fname+'/userTowerInput.csv'
    movieInputPath = './datasets/'+fname+'/movieTowerInput.csv'
    if userInputPath in filesList and movieInputPath in filesList:
        return pd.read_csv(userInputPath), pd.read_csv(movieInputPath)
    else:
        trainDF, testDF = get_DataSetDF(fname)
        movieFixedFeatures = ['movieId','movieGenre1','movieGenre2','movieGenre3']
        userFixFeatures = ['userId',AGE_NAME[fname],'gender','occupation','location']
        all_data = pd.concat([trainDF, testDF], ignore_index=True)
        usersDF = all_data[userFixFeatures].copy()
        usersDF = usersDF.drop_duplicates('userId').sort_values('userId', ignore_index=True)
        moviesDF = all_data[movieFixedFeatures].copy()
        moviesDF = moviesDF.drop_duplicates('movieId').sort_values('movieId', ignore_index=True)
        usersDF.to_csv(userInputPath, index=False)
        moviesDF.to_csv(movieInputPath, index=False)
        return usersDF, moviesDF
        
class modelResults(object):
    
    def __init__(self):
        self.modelNamesSet = {'NeuralCF-DynamicFeatures',
                         'NeuralCF-FixedFeatures',
                         'TwoTowerNeuralCF-DynamicFeatures',
                         'TwoTowerNeuralCF-FixedFeatures',
                         'basicNeuralCF',
                         'basicTwoTowerNeuralCF'}
        self._runningLogs = sorted(walkFiles('./runningLogs', 'log'))
        self._histories = sorted(walkFiles('./histories','pkl'))
        self._tensorboardLogs = sorted(walkFiles('./logs/','v2'))
        self._checkpoints = sorted(walkFiles('./checkpoint/','pb'))
        self.history_dict = {}

    def getSinglePathOfModel(self, fname, modelName):
        
        self.runningLogPath = singleFilePath(fname, modelName, self._runningLogs)
        self.historyPath = singleFilePath(fname, modelName, self._histories)
        self.tensorboardLogPath = singleFilePath(fname, modelName, self._tensorboardLogs, 'train')
        self.checkpointPath = singleFilePath(fname, modelName, self._checkpoints,'saved_model.pb')
#         return self.runningLogPath, self.historyPath, self.tensorboardLogPath, self.checkpointPath
    
    def getPathsByFname(self, fname):
        self.fname = fname
        self.runningLogPaths = getAllPathsByFname(fname, self._runningLogs)
        self.historyPaths = getAllPathsByFname(fname, self._histories)
        self.tensorboardLogPaths = getAllPathsByFname(fname, self._tensorboardLogs)
        self.checkpointPaths = getAllPathsByFname(fname, self._checkpoints)
#         return self.runningLogPaths, self.historyPaths, self.tensorboardLogPaths, self.checkpointPaths
    def loadHistories(self):
        for history in self.historyPaths:
            with open(history, 'rb') as f:
                self.history_dict[history.split('/')[-1][:-4]] = pickle.load(f)
    
    def plotHistories(self, withSubPlot = True):
        figure_order= {}
        figure_order = {k:i+1 for i,k in enumerate(self.history_dict.keys())}
        figure_order['TwoTowerNeuralCF-DynamicFeatures']=4
        figure_order['TwoTowerNeuralCF-FixedFeatures']=5
        figure_order['basicNeuralCF']=3
        if withSubPlot:
            plt.figure(figsize=(15,10))
            for k,v in self.history_dict.items():
                plot(k, self.history_dict, figure_order[k])
            plt.suptitle('RMSE of Different Models on dataset ' + self.fname, fontsize=16)
        else:
            for k,v in self.history_dict.items():
                plot(k, self.history_dict)
        plt.show()
        
        

class tfInputGenerater(object):

    def __init__(self, fname):
        self.fname = fname
        self.batch_size = 128
        if self.fname == 'ml-1m':
            self.batch_size = 256
        self.movieFixedFeatures = ['movieId','movieGenre1','movieGenre2','movieGenre3']
        self.userFixFeatures = ['userId',AGE_NAME[fname],'gender','occupation','location']
        self.userInputPath = './datasets/'+fname+'/userTowerInput.csv'
        self.movieInputPath = './datasets/'+fname+'/movieTowerInput.csv'
        self.users, self.usersDF, _ = self.genereTFdataset(self.userInputPath)
        self.movies, _, self.moviesDF = self.genereTFdataset(self.movieInputPath)
    def genereTFdataset(self, csvPath):
        
        usersDF, moviesDF = generateTwoTowerInputCSV(self.fname)
        tfDataset = tf.data.experimental.make_csv_dataset(
                                                        csvPath,
                                                        batch_size=self.batch_size,
                                                        na_value="0",
                                                        num_epochs=1,
                                                        shuffle=False,
                                                        ignore_errors=True)
        return tfDataset, usersDF, moviesDF

    
class Tower(object):
    
    def __init__(self, fname):
        self.fname = fname
        self.tfDataset = tfInputGenerater(fname)
        results = modelResults()
        results.getSinglePathOfModel(fname, 'TwoTowerNeuralCF-FixedFeatures')
        originalModelPath = results.checkpointPath
        self.originalModel = tf.keras.models.load_model(originalModelPath)
        self.weightLastLayer = float(self.originalModel.weights[-2])
        self.biasLastLayer = float(self.originalModel.weights[-1])
        usersInput = {}
        moviesInput = {}
        for k,v in self.originalModel.input.items():
            if k in self.tfDataset.userFixFeatures:
                usersInput[k] = self.originalModel.input[k]
            if k in self.tfDataset.movieFixedFeatures:
                moviesInput[k] = self.originalModel.input[k]
        self.usersInput = usersInput
        self.moviesInput = moviesInput
        self.userTower = tf.keras.models.Model(inputs=self.usersInput, outputs=self.originalModel.get_layer('gaussian_dropout_1').output)
        self.movieTower = tf.keras.models.Model(inputs=self.moviesInput, outputs=self.originalModel.get_layer('gaussian_dropout').output)
        self._extractEmbedding()
    def _extractEmbedding(self):
        embeddingPath = './datasets/' + self.fname + '/'

        if len(walkFiles(embeddingPath, 'npy'))!=2:
            self.usersEmbeddingMatrix = self.userTower.predict(self.tfDataset.users)
            self.moviesEmbeddingMatrix = self.movieTower.predict(self.tfDataset.movies)
            np.save(embeddingPath + 'usersEmbedding.npy', self.usersEmbeddingMatrix)
            np.save(embeddingPath + 'moviesEmbedding.npy', self.moviesEmbeddingMatrix)
        else:
            self.usersEmbeddingMatrix = np.load(embeddingPath + 'usersEmbedding.npy')
            self.moviesEmbeddingMatrix = np.load(embeddingPath + 'moviesEmbedding.npy')
    def getEmbeddingInfor(self):
        
        print('The shape of usersEmbedding Matrix is:\t', self.usersEmbeddingMatrix.shape)
        print('The shape of moviesEmbedding Matrix is:\t', self.moviesEmbeddingMatrix.shape)
        print('The weight of last layer is:\t', self.weightLastLayer)
        print('The bias of last layer is:\t', self.biasLastLayer)