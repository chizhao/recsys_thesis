#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2/20/21 10:37
# @Author  : zhaochi
# @Email   : dandanv5@hotmail.com
# @File    : helper.py
# @Software: PyCharm
import tensorflow as tf

URL_PREFIX = "https://storage.googleapis.com/thesis_data_rec/"
CSV100K = ['trainingSamples-100k.csv', 'testSamples-100k.csv']
CSV1M = ['trainingSamples-1m.csv', 'testSamples-1m.csv']
AGE_NAME = {'ml-100k': 'ageGroup', 'ml-1m': 'age'}
NUM_BUCKETS = {'movieId_ml-100k': 2000, 'userId_ml-100k': 1000,
               'movieId_ml-1m': 5000, 'userId_ml-1m': 30001}


# load sample as tf dataset
def get_dataset(file_path, batch_size):
    dataset = tf.data.experimental.make_csv_dataset(
        file_path,
        batch_size=batch_size,
        label_name='rating',
        na_value="0",
        num_epochs=1,
        ignore_errors=True)
    return dataset


def getTrainTestDataset(fname: str):
    if fname == 'ml-100k':
        batch_size = 128
        training_samples_file_path = tf.keras.utils.get_file(CSV100K[0], URL_PREFIX + CSV100K[0])
        test_samples_file_path = tf.keras.utils.get_file(CSV100K[1], URL_PREFIX + CSV100K[1])
    elif fname == 'ml-1m':
        batch_size = 256
        training_samples_file_path = tf.keras.utils.get_file(CSV1M[0], URL_PREFIX + CSV1M[0])
        test_samples_file_path = tf.keras.utils.get_file(CSV1M[1], URL_PREFIX + CSV1M[1])
    return get_dataset(training_samples_file_path, batch_size), get_dataset(test_samples_file_path, batch_size)


INPUTS = {
    # This is all the feature columns we may use for Regression Task
    'movieId': tf.keras.layers.Input(name='movieId', shape=(1,), dtype='int32'),
    'userId': tf.keras.layers.Input(name='userId', shape=(1,), dtype='int32'),
    # movies' feature
    'movieAvgRating': tf.keras.layers.Input(name='movieAvgRating', shape=(1,), dtype='float32'),
    # 'movieRatingStddev': tf.keras.layers.Input(name='movieRatingStddev', shape=(1,), dtype='float32'),
    # 'movieRatingCount': tf.keras.layers.Input(name='movieRatingCount', shape=(1,), dtype='int32'),
    # 'releaseYear': tf.keras.layers.Input(name='releaseYear', shape=(1,), dtype='int32'), # this feature has negative effect in four tower model
    'movieGenre1': tf.keras.layers.Input(name='movieGenre1', shape=(1,), dtype='string'),
    'movieGenre2': tf.keras.layers.Input(name='movieGenre2', shape=(1,), dtype='string'),
    'movieGenre3': tf.keras.layers.Input(name='movieGenre3', shape=(1,), dtype='string'),

    # users' behaviour
    # 'userRatedMovie1': tf.keras.layers.Input(name='userRatedMovie1', shape=(1,), dtype='int32'),
    # 'userRatedMovie2': tf.keras.layers.Input(name='userRatedMovie2', shape=(1,), dtype='int32'),
    # 'userRatedMovie3': tf.keras.layers.Input(name='userRatedMovie3', shape=(1,), dtype='int32'),
    # 'userRatedMovie4': tf.keras.layers.Input(name='userRatedMovie4', shape=(1,), dtype='int32'),
    # 'userRatedMovie5': tf.keras.layers.Input(name='userRatedMovie5', shape=(1,), dtype='int32'),
    # 'userReleaseYearStddev': tf.keras.layers.Input(name='userReleaseYearStddev', shape=(1,), dtype='int32'),
    # 'userAvgReleaseYear': tf.keras.layers.Input(name='userAvgReleaseYear', shape=(1,), dtype='int32'),
    'userAvgRating': tf.keras.layers.Input(name='userAvgRating', shape=(1,), dtype='float32'),
    # 'userRatingStddev': tf.keras.layers.Input(name='userRatingStddev', shape=(1,), dtype='float32'),
    # 'userRatingCount': tf.keras.layers.Input(name='userRatingCount', shape=(1,), dtype='int32'),
    'userGenre1': tf.keras.layers.Input(name='userGenre1', shape=(1,), dtype='string'),
    'userGenre2': tf.keras.layers.Input(name='userGenre2', shape=(1,), dtype='string'),
    # 'userGenre3': tf.keras.layers.Input(name='userGenre3', shape=(1,), dtype='string'),
    # 'userGenre4': tf.keras.layers.Input(name='userGenre4', shape=(1,), dtype='string'),
    # 'userGenre5': tf.keras.layers.Input(name='userGenre5', shape=(1,), dtype='string'),

    # user's demographic information
    'age': tf.keras.layers.Input(name='age', shape=(1,), dtype='int32'),  # for ml-1m, should replace dtype='string'
    'ageGroup': tf.keras.layers.Input(name='ageGroup', shape=(1,), dtype='string'),
    # this column is only available for ml-100k
    'gender': tf.keras.layers.Input(name='gender', shape=(1,), dtype='string'),
    'occupation': tf.keras.layers.Input(name='occupation', shape=(1,), dtype='string'),
    'location': tf.keras.layers.Input(name='location', shape=(1,), dtype='string'),
}

AGE_VOCAB = ['Under 18', '18-24', '25-34', '35-44', '45-49', '50-55', 'More than 56']
GENDER_VOCAB = ['M', 'F']
LOCATION_VOCAB = ['AK', 'AL', 'AR', 'AZ', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'IA', 'ID',
                  'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO', 'MS', 'MT',
                  'NC', 'ND', 'NE', 'NH', 'NJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'Other',
                  'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VA', 'VT', 'WA', 'WI', 'WV', 'WY']

# vocabulary for 1m
GENRE_VOCAB_1M = ['Sci-Fi', 'Musical', 'Film-Noir', 'Action', 'Horror', 'Western', 'Mystery', 'Crime', 'War', 'Drama',
                  'Comedy', 'Animation', 'Fantasy', 'Thriller', 'Romance', 'Documentary', "Children's", 'Adventure']

OCCUPATION_VOCAB_1M = ['other', 'academic/educator', 'artist', 'clerical/admin', 'college/grad student',
                       'customer service', 'doctor/health care', 'executive/managerial', 'farmer', 'homemaker',
                       'K-12 student', 'lawyer', 'programmer', 'retired', 'sales/marketing', 'scientist',
                       'self-employed', 'technician/engineer', 'tradesman/craftsman', 'unemployed', 'writer']

# vocabulary for 100k

GENRE_VOCAB_100K = ['unknown', 'Action', 'Adventure', 'Animation', 'Childrens', 'Comedy',
                    'Crime', 'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror',
                    'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western', 'genres']

OCCUPATION_VOCAB_100K = ['administrator', 'artist', 'doctor', 'educator', 'engineer', 'entertainment', 'executive',
                         'healthcare', 'homemaker', 'lawyer', 'librarian', 'marketing', 'none', 'other',
                         'programmer', 'retired', 'salesman', 'scientist', 'student', 'technician', 'writer']
